import java.util.Scanner;

public class ATM {

    public static void main(String[] args) {


        Scanner sc = new Scanner(System.in);

        Bank theBank = new Bank("Your Bank");

        //Adds user savings account
        User aUser = theBank.addUser("Gareth", "Roley", "1234");

        //Adds user checking account
        Account newAccount = new Account("Checking", aUser, theBank);
        aUser.addAccount(newAccount);
        theBank.addAccount(newAccount);

        User curUser;
        while (true) {

            //Does not force login until login is successful
            curUser = ATM.mainMenuPrompt(theBank, sc);

            //Stay in account until terminated by the user
            ATM.printUserMenu(curUser, sc);
        }
    }

    /**
     * Login to the ATM
     * @param theBank Bank accounts
     * @param sc Scanner for user input
     * @return
     */
    public static User mainMenuPrompt(Bank theBank, Scanner sc) {

        String userID;
        String pin;
        User authUser;

        //Prompt user for ID and pin
        do {

            System.out.printf("\n\nWelcome to %s\n\n", theBank.getName());
            System.out.print("Enter account ID: ");
            userID = sc.nextLine();
            System.out.print("Enter PIN: ");
            pin = sc.nextLine();

            authUser = theBank.userLogin(userID, pin);
            if (authUser == null) {
                System.out.println("Incorrect ID and/or PIN. " + "Please try again.");
            }

          //Loops until there is a successful login
        } while(authUser == null);

        return authUser;
    }

    public static void printUserMenu(User theUser, Scanner sc) {

        //Provide a checking and savings account summary
        theUser.printAccountsSummary();

        int choice;

        do {
            System.out.printf("Welcome back %s. How may we assist you?", theUser.getFirstName());
            System.out.println(" 1: Show transaction history");
            System.out.println(" 2: Withdraw Funds");
            System.out.println(" 3: Deposit Funds");
            System.out.println(" 4: Transfer Funds");
            System.out.println(" 5: Exit");
            System.out.println();
            System.out.print("Option: ");
            choice = sc.nextInt();

            if (choice < 1 || choice > 5) {
                System.out.println("Invalid option. Please choose an option as listed above.");
            }
        } while(choice < 1 || choice > 5);

        switch (choice) {

            //Output in response to user input
            case 1:
                ATM.showTransHistory(theUser, sc);
                break;
            case 2:
                ATM.withdrawFunds(theUser, sc);
                break;
            case 3:
                ATM.depositFunds(theUser, sc);
                break;
            case 4:
                ATM.transferFunds(theUser, sc);
                break;
        }

        //Show menu options again using recursion
        if (choice != 5) {
            ATM.printUserMenu(theUser, sc);
        }
    }

    /**
     * Show account transaction history
     * @param theUser
     * @param sc
     */
    public static void showTransHistory(User theUser, Scanner sc) {

        int theAcct;

        //Prompt user to specify account for transaction history
        do {
            System.out.printf("Please enter account number (1-%d)" + "for target account", theUser.numAccounts());
            theAcct = sc.nextInt()-1;
            if(theAcct < 0 || theAcct >= theUser.numAccounts()) {
                System.out.println("This is an invalid account. Please try again.");
            }

        } while (theAcct < 0 || theAcct >= theUser.numAccounts());

        //Print transaction history
        theUser.printAcctTransHistory(theAcct);
    }

    public static void transferFunds(User theUser, Scanner sc) {

        int fromAcct;
        int toAcct;
        double amount;
        double acctBal;

        //Transferring account info
        do {
            System.out.printf("Enter the account (1-%d) you wish to transfer from: ", theUser.numAccounts());
            fromAcct = sc.nextInt()-1;
            if (fromAcct < 0 || fromAcct >= theUser.numAccounts()) {
                System.out.println("This is an invalid account. Please try again.");
            }
        } while (fromAcct < 0 || fromAcct >= theUser.numAccounts());
        acctBal = theUser.getAcctBalance(fromAcct);

        //What account are funds transferring to
        do {
            System.out.printf("Enter the account (1-%d) you wish to transfer to: ", theUser.numAccounts());
            toAcct = sc.nextInt()-1;
            if (toAcct < 0 || toAcct >= theUser.numAccounts()) {
                System.out.println("This is an invalid account. Please try again.");
            }
        } while (toAcct < 0 || toAcct >= theUser.numAccounts());

        //Amount that was transferred\
        do {
            System.out.printf("Enter amount (max $%.02f): $", acctBal);
            amount = sc.nextDouble();
            if (amount < 0) {
                System.out.println("Enter an amount greater than zero.");
            } else if (amount > acctBal) {
                System.out.printf("Amount must not be greater than your account balance of $%.02f", acctBal);
            }
        } while (amount < 0 || amount > acctBal);

        //Transfer funds
        theUser.addAcctTransaction(fromAcct, -1*amount, String.format("Transfer to account %s", theUser.getAcctUUID(toAcct)));
        theUser.addAcctTransaction(toAcct, amount, String.format("Transfer to account %s", theUser.getAcctUUID(fromAcct)));
    }

    /**
     * Withdraw funds
     * @param theUser
     * @param sc
     */
    public static void withdrawFunds(User theUser, Scanner sc) {

        int fromAcct;
        double amount;
        double acctBal;
        String memo;

        //Transferring account info
        do {
            System.out.printf("Enter the account number (1-%d) you wish to withdraw from: ", theUser.numAccounts());
            fromAcct = sc.nextInt()-1;
            if (fromAcct < 0 || fromAcct >= theUser.numAccounts()) {
                System.out.println("This is an invalid account. Please try again.");
            }
        } while (fromAcct < 0 || fromAcct >= theUser.numAccounts());
        acctBal = theUser.getAcctBalance(fromAcct);

        //Amount that was transferred\
        do {
            System.out.printf("Enter amount (max $%.02f): $", acctBal);
            amount = sc.nextDouble();
            if (amount < 0) {
                System.out.println("Enter an amount greater than zero.");
            } else if (amount > acctBal) {
                System.out.printf("Amount must not be greater than your account balance of $%.02f", acctBal);
            }
        } while (amount < 0 || amount > acctBal);

        sc.nextLine();

        //Enter a memo
        System.out.println("Enter a memo:");
        memo = sc.nextLine();

        //Withdraw
        theUser.addAcctTransaction(fromAcct,-1*amount, memo);
    }

    /**
     * Deposit funds
     * @param theUser
     * @param sc
     */
    public static void depositFunds(User theUser, Scanner sc) {

        int toAcct;
        double amount;
        double acctBal;
        String memo;

        //Transferring account info
        do {
            System.out.printf("Enter the account number (1-%d) you wish to deposit in: ", theUser.numAccounts());
            toAcct = sc.nextInt()-1;
            if (toAcct < 0 || toAcct >= theUser.numAccounts()) {
                System.out.println("This is an invalid account. Please try again.");
            }
        } while (toAcct < 0 || toAcct >= theUser.numAccounts());
        acctBal = theUser.getAcctBalance(toAcct);

        //Amount that was transferred
        do {
            System.out.printf("Enter amount (max $%.02f): $", acctBal);
            amount = sc.nextDouble();
            if (amount < 0) {
                System.out.println("Enter an amount greater than zero.");
            }
        } while (amount < 0);

        sc.nextLine();

        //Enter a memo
        System.out.println("Enter a memo:");
        memo = sc.nextLine();

        //Withdraw
        theUser.addAcctTransaction(toAcct, amount, memo);
    }
}
