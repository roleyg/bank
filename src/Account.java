import java.util.ArrayList;

public class Account {

        //The name of the account
        private String name;

        //The account number
        private String uuid;

        //User that owns the account
        private User holder;

        //List of transactions
        private ArrayList<Transaction> transactions;

        /**New account
        *The account name
         * Object that holds the account
         * Bank that provides account
         * */
        public Account(String name, User holder, Bank theBank) {

            //Account name
            this.name = name;
            this.holder = holder;

            //Account UUID
            this.uuid = theBank.getNewAccountUUID();

            //Transactions
            this.transactions = new ArrayList<Transaction>();

        }

    /**
     *
     * @return uuid for account in Bank
     */
    public String getUUID() {
            return this.uuid;
    }

    /**
     * Summary for account
     * @return
     */
    public String getSummaryLine() {

        //Get account balance
        double balance = this.getBalance();

        //For negative balance
        if (balance >= 0) {
            return String.format("%s : $%.02f : %s", this.uuid, balance, this.name);
        } else {
            return String.format("%s : $(%.02f) : %s", this.uuid, balance, this.name);
        }
    }

    /**
     * Provide account balance
     * @return The balance
     */
    public double getBalance() {

        double balance = 0;
        for (Transaction t : this.transactions) {
            balance += t.getAmount();
        }
        return balance;
    }

    /**
     * Print account transaction history
     */
    public void printTransHistory() {

        System.out.printf("\nTransaction history for account %s\n", this.uuid);
        for (int t = this.transactions.size() - 1; t >= 0; t--) {
            System.out.printf(this.transactions.get(t).getSummaryLine());
        }
        System.out.println();
    }

    /**
     *Transaction for accounts
     * @param amount
     * @param memo
     */
    public void addTransaction(double amount, String memo) {

        //Transaction obj
        Transaction newTrans = new Transaction(amount, memo, this);
        this.transactions.add(newTrans);
    }

}
