import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.security.MessageDigest;

public class User {

    //Here the user will enter in their first name
    private String firstName;

    //Here the user will enter in the last name
    private String lastName;

    //This is the user ID
    private String uuid;

    //Has for user's pin number
    private byte pinHash[];

    //The accounts associated with the user
    private ArrayList<Account> accounts;

    public User(String firstName, String lastName, String pin, Bank theBank) {

        //User's name
        this.firstName = firstName;
        this.lastName = lastName;

        //Store pin hash
        //Try and Catch to handle error
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            this.pinHash = md.digest(pin.getBytes());
        } catch (NoSuchAlgorithmException e) {
            System.err.println("error, caught NoSuchAlgorithmException");
            e.printStackTrace();
            System.exit(1);
        }

        //Provides unique ID for the user
        this.uuid = theBank.getNewUserUUID();

        //List of accounts
        this.accounts = new ArrayList<Account>();

        //Print message
        System.out.printf("New user %s, %s with ID %s created.\n", lastName, firstName, this.uuid);


    }

    /**Add user account
     *
     * @param anAcct this is the account to add
     */
    public void addAccount(Account anAcct) {
        this.accounts.add(anAcct);
    }

    /**
     *
     * @return the uuid for user Bank
     */
    public String getUUID() {
        return this.uuid;
    }

    /**
     *
     * @param aPin The pin
     * @return Check for a valid pin
     */
    public boolean validatePin(String aPin) {

        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            return MessageDigest.isEqual(md.digest(aPin.getBytes()), this.pinHash);
        } catch (NoSuchAlgorithmException e) {
            System.err.println("error, caught NoSuchAlgorithmException");
            e.printStackTrace();
            System.exit(1);
        }

        return false;
    }

    public String getFirstName() {
        return this.firstName;
    }

    /**
     * Print summary for account
     */
    public void printAccountsSummary() {

        System.out.printf("\n\n%s's account summary", this.firstName);
        for (int a = 0; a < this.accounts.size(); a++) {
            System.out.printf("%d) %s\n", a+1, this.accounts.get(a).getSummaryLine());

        }
        System.out.println();
    }

    /**
     * Provide the number of user accounts
     * @return
     */
    public int numAccounts() {
        return this.accounts.size();
    }

    /**
     * Prints transaction history for a specified account
     * @param acctIdx
     */
    public void printAcctTransHistory(int acctIdx) {
        this.accounts.get(acctIdx).printTransHistory();
    }

    public double getAcctBalance(int acctIdx) {
        return this.accounts.get(acctIdx).getBalance();
    }

    /**
     * Get account ID
     * @param acctIdx Account index
     * @return
     */
    public String getAcctUUID(int acctIdx) {
        return this.accounts.get(acctIdx).getUUID();
    }

    public void addAcctTransaction(int acctIdx, double amount,String memo) {
        this.accounts.get(acctIdx).addTransaction(amount, memo);
    }

}
