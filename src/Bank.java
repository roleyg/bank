import java.util.ArrayList;
import java.util.Random;

public class Bank {

    private String name;

    private ArrayList<User> users;

    private ArrayList<Account> accounts;

    /**
     *
     * @param name
     */
    public Bank(String name) {

        this.name = name;
        this.users = new ArrayList<User>();
        this.accounts = new ArrayList<Account>();
    }

    /**
     * Generates an ID for the user
     * @return the UUID
     */
    public String getNewUserUUID() {

        String uuid;
        Random rng = new Random();
        int len = 4;
        boolean nonUnique;

        //Continues loop until a unique ID has been made
        do {

            uuid = "";
            for (int c = 0; c < len; c++) {
                uuid += ((Integer)rng.nextInt(10)).toString();
            }

            //Check unique uuid
            nonUnique = false;
            for(User u : this.users) {
                if (uuid.compareTo(u.getUUID()) == 0) {
                    nonUnique = true;
                    break;
                }
            }

        } while (nonUnique);

        return uuid;

    }

    public String getNewAccountUUID() {

        String uuid;
        Random rng = new Random();
        int len = 8;
        boolean nonUnique;

        //Continues loop until a unique ID has been made
        do {

            uuid = "";
            for (int c = 0; c < len; c++) {
                uuid += ((Integer)rng.nextInt(10)).toString();
            }

            //Check unique uuid
            nonUnique = false;
            for(Account a : this.accounts) {
                if (uuid.compareTo(a.getUUID()) == 0) {
                    nonUnique = true;
                    break;
                }
            }

        } while (nonUnique);

        return uuid;
    }

    /**
     *
     * @param onAcct This is the account to add
     */
    public void addAccount(Account onAcct) {
        this.accounts.add(onAcct);
    }

    /**
     *
     * @param firstName User first name
     * @param lastName User last name
     * @param pin User account pin
     * @return
     */
    public User addUser(String firstName, String lastName, String pin) {

        //Creates user object
        User newUser = new User(firstName, lastName, pin, this) ;
        this.users.add(newUser);

        //Creates a savings account for user and bank
        Account newAccount = new Account("Savings", newUser, this);
        newUser.addAccount(newAccount);
        this.accounts.add(newAccount);

        return newUser;
    }

    /**
     *
     * @param userID
     * @param pin
     * @return
     */
    public User userLogin(String userID, String pin) {

        //Search for users
        for (User u : this.users) {

            //Check for valid ID
            if (u.getUUID().compareTo(userID) == 0 && u.validatePin(pin)) {
                return u;
            }
        }

        //If ID or pin is not valid
        return null;
    }

    /**
     * Bank name
     * @return
     */
    public String getName() {
        return this.name;
    }
}
