import java.util.Date;

public class Transaction {

    //Transaction amount
    private double amount;

    //Time and date of the transaction
    private Date timestamp;

    //Note for the transaction
    private String memo;

    //What account did the transaction take place
    private Account inAccount;

    /**
     *
     * @param amount Transaction amount
     * @param inAccount What account is performing the transaction
     */
    public Transaction (double amount, Account inAccount) {

        this.amount = amount ;
        this.inAccount = inAccount;
        this.timestamp = new Date();
        this.memo = "";

    }

    /**
     *
     * @param amount Transaction amount
     * @param memo Transaction memo
     * @param inAccount Account that the transaction belongs to
     */
    public Transaction(double amount, String memo, Account inAccount) {

        //
        this(amount, inAccount);

        //Set memo
        this.memo = memo;
    }

    public double getAmount () {
        return this.amount;
    }

    /**
     * String to summarize the transaction
     * @return the summary string
     */
    public String getSummaryLine() {

        if (this.amount >= 0) {
            return String.format("%s : $%.02f : %s", this.timestamp.toString(), this.amount, this.memo);

        } else {
            return String.format("%s : $(%.02f) : %s", this.timestamp.toString(), this.amount, this.memo);
        }
    }
}
